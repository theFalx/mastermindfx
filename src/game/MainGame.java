package game;

import game.menu.MainMenu;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by falx on 3/24/14.
 */
public class MainGame
{
    /******************************************************************************/
    //                          static class part                                 //
    /******************************************************************************/
    private static Stage              stage;
    private static MainGameController mainGameController;

    public  static final int MASTERMIND = 0x0;
    public  static final int SUPERMIND  = 0x1;

    public  static final int MASTERMIND_COLOR_AMOUNT = 6;
    public  static final int SUPERMIND_COLOR_AMOUNT  = 8;
    public  static final int GUESSING_AMOUNT         = 12;

    public  static final int MASTERIND_CODE_SIZE = 4;
    public  static final int SUPERMIND_CODE_SIZE = 5;

    public static final Color   DEFAULT_COLOR   = Color.GRAY;
    public static final Color[] POSSIBLE_COLORS = {
            Color.RED,
            Color.BLUE,
            Color.GREEN,
            Color.YELLOW,
            Color.MAGENTA,
            Color.ORANGE,
            Color.LIME,
            Color.TURQUOISE,
    };
    public static final Color[] CONTROL_COLORS = {
            Color.WHITE, // right color, right position;
            Color.RED,   // right color, wrong position
    };

    private static       int initMode;


    public static void launch( Stage stage, int mode )
    {
        MainGame.stage    = stage;
        MainGame.initMode = mode;
        MainGame game = new MainGame();
        game.init();
    }

    /******************************************************************************/
    //                          member class part                                 //
    /******************************************************************************/

    private MainGame()
    {
        /* nothing to do here! */
    }

    private void init()
    {
        Parent     root = null;
        Scene      scene;
        FXMLLoader fxmlLoader;

        fxmlLoader = new FXMLLoader( MainGame.class.getResource("res/MainGame.fxml") );
        try
        {
            root = fxmlLoader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            MainMenu.hide();
        }

        root.getStylesheets().add( MainGame.class.getResource("res/MainGame.css").toExternalForm() );
        scene = new Scene( root, 320, 800 );
        MainGame.stage.setScene( scene );
        mainGameController =  fxmlLoader.getController();
        mainGameController.init( initMode );

        MainGame.stage.show();
    }

    public static void backToMenu()
    {
        MainGame.stage.hide();
        MainMenu.show();
    }

}
