package game.menu;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainMenu extends Application
{
    /******************************************************************************/
    //                          static class part                                 //
    /******************************************************************************/
    private static Stage              primaryStage;
    private static MainMenuController mainMenuController;

    public static void show() throws IllegalStateException
    {
        if( primaryStage == null )
            throw new IllegalStateException( "MainMenu must launch first!" );

        primaryStage.show();
    }

    public static void hide() throws IllegalStateException
    {
        if( primaryStage == null )
            throw new IllegalStateException( "MainMenu must launch first!" );

        primaryStage.hide();
    }

    public static void main(String[] args)
    {
        launch(args);
    }

    /******************************************************************************/
    //                          member stuff part                                 //
    /******************************************************************************/

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        FXMLLoader         fxmlLoader;
        Parent             root;
        MainMenuController controller;

        fxmlLoader = new FXMLLoader(getClass().getResource("ressourcen/MainMenu.fxml"));
        root       = fxmlLoader.load();
        controller = fxmlLoader.getController();

        root.getStylesheets().add( MainMenu.class.getResource( "ressourcen/MainMenu.css" ).toExternalForm() );

        MainMenu.primaryStage = primaryStage;
        mainMenuController    = controller;

        primaryStage.setTitle("Mastermind");
        primaryStage.setScene(new Scene(root, 400, 375));
        primaryStage.setResizable( false );
        primaryStage.show();
    }

}
