package game.menu;

import game.MainGame;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainMenuController
{
    @FXML
    GridPane gridPaneRoot;
    @FXML
    ToggleGroup toggleGroup; // get gametype with this group?!?
    @FXML
    RadioButton rbtnMastermind;
    @FXML
    RadioButton rbtnSupermind;

    @FXML
    public void startGame( ActionEvent event )
    {
        System.out.println( "test" );
        System.out.println( ((Button)event.getSource()).getText() );
        System.out.println( gridPaneRoot.getScene().getHeight() );

        System.out.printf( "Mastermind: %b\n", rbtnMastermind.isSelected() );
        System.out.printf( "Supermind: %b\n", rbtnSupermind.isSelected() );
        System.out.println( MainGame.MASTERMIND == MainGame.SUPERMIND );

        int gameType = rbtnMastermind.isSelected() ? MainGame.MASTERMIND : MainGame.SUPERMIND;
        MainMenu.hide();
        MainGame.launch( new Stage(), gameType );
    }

}
