package game;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by falx on 3/25/14.
 */
public class MainGameController implements EventHandler<MouseEvent>
{
    /******************************************************************************/
    //                          static stuff part                                 //
    /******************************************************************************/


    /******************************************************************************/
    //                       fxml declaration  stuff part                         //
    /******************************************************************************/

    // variables - code injection //

    @FXML
    BorderPane rootPane;
    @FXML
    MenuBar mBar;
    @FXML
    HBox masterCodeHBox;
    @FXML
    VBox codeVBox;
    @FXML
    GridPane contentVBox;
    @FXML
    Button confirmBtn;
    @FXML
    Text msgText;

    // event-handler //

    @FXML
    public void confirmActionHandle( ActionEvent actionEvent )
    {
        if( !checkIfValidGuess() )
            msgText.setText( "You have to choose colors first!" );
        else
        {
            msgText.setText( "" );
            boolean won = control(); // --> implement control function
            if( currentId != MainGame.GUESSING_AMOUNT - 1  )
            {
                gameField[currentId].setOpacity(0.66d);
                currentId = (currentId + 1);
                gameField[currentId].setOpacity(1.0d);
            }

            if( won )
            {
                msgText.setText("You have won!");
                showDialog( "You have won!" );
                exit();
            }
            else {
                if (currentId == MainGame.GUESSING_AMOUNT - 1)
                {
                    msgText.setText("You have lost!");
                    showDialog( "You have lost!" );
                    exit();
                }
            }

        }
    }

    @FXML
    private void exit()
    {
        MainGame.backToMenu();
    }
    /******************************************************************************/
    //                          member stuff part                                 //
    /******************************************************************************/

    // variables //

    private Circle[][]  circleMap;
    private Circle[][]  controlCircleMap;
    private HBox[]      gameField;
    private int         gameMode;
    private int         codeSize;
    private int         colorSize;
    private int         currentId;
    private List<Color> activeColors;
    private Color[]     masterCode;

    // init methods //

    public void init( int gameMode )
    {
        this.gameMode = gameMode;

        if( gameMode == MainGame.MASTERMIND )
        {
            this.codeSize = MainGame.MASTERIND_CODE_SIZE;
            this.colorSize = MainGame.MASTERMIND_COLOR_AMOUNT;
        }
        else if ( gameMode == MainGame.SUPERMIND )
        {
            this.codeSize  = MainGame.SUPERMIND_CODE_SIZE;
            this.colorSize = MainGame.SUPERMIND_COLOR_AMOUNT;
        }
        else
            throw new IllegalArgumentException( "Unknown gameMode!" );

        currentId = 0;

        setUpActiveColors();
        setUpGameCircles();
        setUpControlCircles();
        setUpGameField();
        setUpMasterCode();

        mBar.prefWidthProperty().bind( rootPane.getScene().widthProperty() );
        contentVBox.prefWidthProperty().bind( rootPane.getScene().widthProperty() );
        //codeVBox.prefWidthProperty().bind( contentVBox.widthProperty() );
        //codeVBox.setFillWidth( true );
    }


    // EventHandler //

    @Override
    public void handle( MouseEvent mouseEvent )
    {
        Circle circle = (Circle)mouseEvent.getSource();
        if( Integer.parseInt( circle.getId() ) != currentId )
            return;

        int colorIndex  = activeColors.indexOf( circle.getFill() ) + 1 ;
            colorIndex %= activeColors.size();
        circle.setFill( activeColors.get( colorIndex ) );
    }

    /******************************************************************************/
    //                        private method part                                 //
    /******************************************************************************/

    // init methods //

    private void setUpActiveColors()
    {
        activeColors = new ArrayList<>(codeSize+1);
        for( int i = 0; i < colorSize; i++ )
            activeColors.add( MainGame.POSSIBLE_COLORS[i] );
    }

    private void setUpGameCircles()
    {
        this.circleMap = new Circle[MainGame.GUESSING_AMOUNT][codeSize];

        for( int i = 0; i < MainGame.GUESSING_AMOUNT; i++ ) {
            for( int j = 0; j < codeSize; j++ )
            {
                Circle c = new Circle( 15 );
                c.setFill( MainGame.DEFAULT_COLOR );
                c.setId(Integer.toString(i));
                c.setOnMouseClicked(this);
                c.getStyleClass().add("circle");

                circleMap[i][j] = c;
            }
        }
    }

    private void setUpGameField()
    {
        this.gameField = new HBox[MainGame.GUESSING_AMOUNT];
        for( int i = 0; i < gameField.length; i++ )
        {
            HBox hBox = new HBox();
            hBox.setSpacing( 25 );
            hBox.setId( Integer.toString( i ) );
            hBox.getChildren().addAll( circleMap[i] );
            hBox.getChildren().add( setUpControlPane( i ) );
            if( i != currentId )
                hBox.setOpacity( 0.5d );


            gameField[i] = hBox;
            codeVBox.getChildren().add(gameField[i]);
        }
    }


    private void setUpControlCircles()
    {
        this.controlCircleMap = new Circle[MainGame.GUESSING_AMOUNT][codeSize];

        for( int i = 0; i < controlCircleMap.length; i++ ) {
            for( int j = 0; j < codeSize; j++ )
            {
                Circle c = new Circle( 5 );
                //c.setFill( MainGame.DEFAULT_COLOR );
                c.setFill( Color.LIGHTGRAY );
                //c.getStyleClass().add( "circle" );
                controlCircleMap[i][j] = c;
            }
        }
    }

    private GridPane setUpControlPane( int rowIndex )
    {
        GridPane gridPane = new GridPane();
        Circle[] circles  = controlCircleMap[rowIndex];

        gridPane.add( circles[0], 0, 0 );
        gridPane.add( circles[1], 3, 0 );
        if( gameMode == MainGame.SUPERMIND )
        {
            gridPane.add( circles[2], 2, 1 );
            gridPane.add( circles[3], 0, 2 );
            gridPane.add( circles[4], 3, 2 );
        }
        else
        {
            gridPane.add( circles[2], 0, 2 );
            gridPane.add( circles[3], 3, 2 );
        }
        return gridPane;
    }

    private void setUpMasterCode()
    {
        this.masterCode = new Color[codeSize];
        for( int i = 0; i < codeSize; i++ )
        {
            int colorIndex = (int)(Math.random() * 100) % ( codeSize );
            masterCode[i] = activeColors.get( colorIndex );
        }

        System.out.println( Arrays.toString( masterCode ) );
    }

    private boolean control()
    {
        boolean  won    = true;
        Color[]  cntrl  = new Color[codeSize];
        Color[]  master = Arrays.copyOf( masterCode, masterCode.length );
        Circle[] code   = circleMap[currentId];

        final Color WHITE = MainGame.CONTROL_COLORS[0]; // right color, right position
        final Color RED   = MainGame.CONTROL_COLORS[1]; // right color, wrong position

        int cntrlIndex = 0;


        // check if right color on right position: -> white
        for( int i = 0; i < codeSize; i++ ) {
            if( code[i].getFill().equals( master[i] ) )
            {
                cntrl[cntrlIndex] = WHITE;
                cntrlIndex++;
                code[i]   = null;
                master[i] = null;

                won &= true;
            }
            else
                won &= false;
        }

        // check if right color: -> black
        for( int i = 0; i < codeSize; i++ ) {
            if( code[i] != null )
            { System.out.println( "not null " );
                Paint fill = code[i].getFill();
                for( int j = 0; j < codeSize; j ++ ) {
                    if( master[j] != null ) {
                        if( master[j].equals( fill ) )
                        {
                            cntrl[cntrlIndex] = RED;
                            cntrlIndex++;
                            master[j] = null;
                            code[i]   = null;
                        }
                    }
                }
            }
        }


        for( int i = 0; i < cntrl.length; i++ ) {
            if( cntrl[i] != null )
            {
                controlCircleMap[currentId][i].setFill( cntrl[i] );
                //controlCircleMap[currentId][i].setFill( Color.WHITE );
            }
        }

        return won;
    }

    private boolean checkIfValidGuess()
    {
        boolean valid = true;

        for( int i = 0; i < codeSize; i++ )
            valid &= circleMap[currentId][i].getFill() != MainGame.DEFAULT_COLOR;

        return valid;
    }


    private static void showDialog( String  s )
    {
        Stage stg  = new Stage();
        VBox  vbox = new VBox();
        Button btn = new Button( "OK");
        Scene sc   = new Scene( vbox );

        vbox.getChildren().add( new Text( s ) );
        vbox.setAlignment( Pos.CENTER );
        btn.setOnAction( (ActionEvent actionEvent) ->
        {
            stg.hide();
        });
        vbox.getChildren().add(btn);

        stg.initModality( Modality.WINDOW_MODAL );
        stg.setScene( sc );
        stg.showAndWait();
    }
}
